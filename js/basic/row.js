function Row() {
	this.index;
	this.columns = [];
	this.selectedColumn;
}

Row.prototype = {
	getIndex: function() {
		return this.index;
	},
	setIndex: function(newIndex) {
		this.index = newIndex;
	},
	getColumn: function(number) {
		return this.columns[number];
	},
	setColumn: function(number, content) {
		this.columns[number] = content;
	},
	getSelectedColumn: function() {
		return this.selectedColumn;
	},
	setSelectedColumn: function(indx) {
		this.selectedColumn = indx;
	}
};

