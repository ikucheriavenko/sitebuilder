$('#add-content-btn').click(function(e) {
	var rowIndex = addRowToStorage();
	buildRow(rowIndex);
});

$('#delete-content-btn').click(function(e) {
	var rowIndex = destructRow();
	removeRowFromStorage(rowIndex);
});

$('body').on('click', 'div.content-block', function() {
	changeActiveRow($(this));
});

$('.col-number').click(function(e) {
	buildContentColumn($(this).attr('data-value'));
});


////////modal dialogs////////
$('body').on('click', 'span.edit-btn', function() {
	$('#edit-dialog').modal('toggle');
	var column = $(this).parent();
	var row = column.parent();
	prepareColumnForSavingContent(row, column);
});

// add text
$('#add-text').click(function(e) {
	$('#text-dialog').modal('toggle');
	$('#edit-dialog').modal('toggle');
});

$('#btn-insert-text').click(function(e) {
	addContentToColumn($('#tx-ar-content').val());
	rebuildRow();
});

//add image
$('#add-image').click(function(e) {
	$('#image-upload-dialog').modal('toggle');
	$('#edit-dialog').modal('toggle');
});

$('#btn-insert-image').click(function(e) {
	addContentToColumn($('#tx-ar-content').val());
	rebuildRow();
});


$('#add-video').click(function(e) {
	$('#video-upload-dialog').modal('toggle');
	$('#edit-dialog').modal('toggle');
});

$('#add-wysiwyg').click(function(e) {
	$('#wysiwyg-dialog').modal('toggle');
	$('#edit-dialog').modal('toggle');
});
$('#wysiwyg-textarea').wysihtml5();

// image upload
$("#upload-image").change(function(){
  $("#img").css({top: 0, left: 0});
    preview(this);
    $("#img").draggable({ containment: 'parent',scroll: false });
});
//video upload
$("#upload-video").change(function(){
  $("#img").css({top: 0, left: 0});
    preview(this);
    $("#img").draggable({ containment: 'parent',scroll: false });
});