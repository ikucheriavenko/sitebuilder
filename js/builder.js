function buildRow(index) {
	var newBlock = $('.content-block-pattern').clone();
	newBlock.removeClass('content-block-pattern');
	newBlock.addClass('content-block');
	newBlock.attr('data-row', index);
	newBlock.css('display', 'block');
	$('.page-content').append(newBlock);
}

function destructRow() {
	if ($('.content-block').length > minContentBlockNumber) {
		$('.content-block').last().remove();
		return $('.content-block').attr('data-row');
	}	
}

function changeActiveRow(block) {
	//change border color to default - blue for all
	$('body').find('.border-active').each(function() {
  		$(this).removeClass('border-active');
  		$(this).addClass('border');
	});

	if (block.hasClass('active-content-block')) {
		block.toggleClass('active-content-block');
		return;
	}

	$('.content-block').removeClass('active-content-block');
	block.addClass('active-content-block');

	//change color for active block
	block.find('.content').each(function() {
		$(this).addClass('border-active');
	});
}

function buildContentColumn(amount) {
	//remove old grids inside
	$('.active-content-block').children().remove();
	// add new columns
	for (var i = 0; i < amount; i++) {
		var newContent = $('.content-block-pattern').find('.content').clone();
		newContent.attr('data-column', i+1);  // column number starts from 1.
		$('.active-content-block').append(newContent);
	}

	$('.active-content-block .content').addClass('col-xs-' + 12/amount);
	// $('.active-content-block .content').attr('data-index', );                   /////////????
	$('.active-content-block .content:not(:first)').addClass('border-active');
}


function rebuildRow() {
	var row = JSON.parse(localStorage.getItem('toRebuild'));
	row.__proto__ = Row.prototype;
	var content = row.getColumn(row.getSelectedColumn());
	insertTextContent(row.getIndex(), row.getSelectedColumn(), content);
	
}

function insertTextContent(rowIndex, columnIndex, content) {
	var textContainer = $("div[data-row='" + rowIndex +"']")
						.find("div[data-column='" + columnIndex +"']")
						.find('.content-container');
	textContainer.parent().removeClass('center-align');
	textContainer.parent().addClass('left-align');
	textContainer.html(content);
}



