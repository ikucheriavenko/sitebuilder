function addRowToStorage() {	
	var index = getIndex();
	var row = new Row();
	row.setIndex(index);
	localStorage.setItem(index, JSON.stringify(row));
	return index;
}

function removeRowFromStorage(index) {
	localStorage.removeItem(index);
}

function prepareColumnForSavingContent(row, column) {
	rowIndex = row.attr('data-row');
	colIndex = column.attr('data-column');
	var row = JSON.parse(localStorage.getItem(rowIndex));
	row.__proto__ = Row.prototype;
	row.setSelectedColumn(colIndex);
	localStorage.setItem('toRebuild', JSON.stringify(row));
}

function addContentToColumn(content) {
	var row = JSON.parse(localStorage.getItem('toRebuild'));
	row.__proto__ = Row.prototype; 
	row.setColumn(row.getSelectedColumn(), content);
	localStorage.setItem('toRebuild', JSON.stringify(row));
	localStorage.setItem(row.getIndex(), JSON.stringify(row));
}

function getIndex() {
	var index = +localStorage.getItem('index') + 1;
	localStorage.setItem('index', index);
	return index;
}

